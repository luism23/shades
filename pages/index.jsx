import Content from "@/components/Content"
import Banner from "@/components/banner"
import Section from "@/components/section"
import Features from "@/components/features"
import Features2 from "@/components/features2"
import Clients from "@/components/clients"
import Logos from "@/components/logos"
import Servicepackage from "@/components/servicepackage"

import Info from "@/data/pages/index"

const Index = () => {
    return (
        <>
            <Content>
                <Banner {...Info.banner}/>
                <Section {...Info.section}/>
                <Features {...Info.features}/>
                <Features2 {...Info.features2}/>
                <Clients {...Info.clients}/>
                <Logos {...Info.logos}/>
                <Servicepackage {...Info.servicepackage}/>
            </Content>
        </>
    )
}
export default Index