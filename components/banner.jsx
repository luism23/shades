import Link from "next/link"

const Index = ({ subTitle = "", title = "", text = "", text1 = "",link="",linkText="" }) => {
    return (
        <>
            <div className="bg-white-2  ">
                <div className="container p-h-15 p-t-257 p-b-157">
                    <div className="text-center ">
                        <h4 className="color-gray font-w-700  font-karla font-15 m-b-27">
                            {subTitle}
                        </h4>
                        <h1 className="color-black-2 font-karla font-w-700 font-70 m-b-19">
                            {title}
                        </h1>
                        <p className="color-gray font-karla  font-21 m-b-31">
                            {text}
                        </p>
                        <div className="flex m-b-24 flex-justify-center">
                            <input type="email" className="login-input p-v-20 p-l-28 p-r-87 m-b-10 m-r-11 color-gray bg-white border-0" placeholder="Enter your email" />
                            <Link href={link}>
                                <a className=" text-center p-v-20 p-h-78 color-white bg-orangle border-0">  
                                    {linkText}
                                </a>
                            </Link>
                        </div>
                        <div className="">
                            <p className="color-gray font-karla  font-17">
                                {text1}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index