import Link from "next/link"
import DATA from "@/data/components/Header"


import Data from "@/data/components/Header"
import { useEffect, useRef, useState } from "react"
import Menu from "@/svg/menu"

const LinkTitle = ({ title = "", link = "" }) => {
    return (
        <>
            <div className="m-auto">
                <Link href={link}>
                    <a className="font-16 font-karla color-black-2 m-lg-r-36 text-center">
                        {title}
                    </a>
                </Link>
            </div>
        </>
    )
}


const Index = () => {
    const { linkTitle, img,link ,linkText} = Data
    const header = useRef(null)
    const [heightMenu, setHeightMenu] = useState(0)
    useEffect(() => {
        const height = header.current.offsetHeight
        setHeightMenu(height)
    }, [])
    return (
        <>
            <header ref={header} className="header  pos-f bg-white-2 top-0 left-0 width-p-100  flex flex-align-center p-v-45">
                <div className="container p-h-15 flex flex-justify-between">
                    <div className="flex-2">
                        <img className="flex " src={`/image/${img}`} alt="" />
                    </div>
                    <div className="border-radius-50 flex-10 d-lg-none flex flex-justify-right">
                        <button
                            className="color-blue-1 bg-white-2 border-0"
                            onClick={() => {
                                document.body.classList.toggle("activeMenu")
                            }}>
                            <Menu size={34} className="border-radius-50 " />
                        </button>
                    </div>
                    <div
                        className="menuMovil flex-justify-center bg-white-2 flex-12 flex-lg-7 flex"
                        style={{ "--heightMenu": heightMenu + "px" }}
                        onClick={(e) => {
                            if (e.target.tagName == "A") {
                                document.body.classList.toggle("activeMenu")
                            }
                        }}
                    >
                        <div className="flex-8 flex flex-justify-right col-1">
                            {
                                linkTitle.map((e, i) => {
                                    return (
                                        <LinkTitle
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                        <div className="flex-12 flex-lg-2 flex flex flex-justify-center  ">

                            <div className="flex flex-align-center">
                                <Link href={link}>
                                    <a className=" p-t-10 p-b-10 p-h-30 font-16 font-w-700 font-karla border-style-solid color-blue-1  ">{linkText}</a>
                                </Link>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
        </>
    )
}
export default () => (<Index {...DATA} />)