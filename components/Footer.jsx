import Link from "next/link"


import DATA from "@/data/components/Footer"

const LinkTitle = ({ link = "", linkText = "" }) => {
    return (
        <>
            <div className="m-b-26">
                <Link href={link}>
                    <a className="color-black-2 font-w-700 font-karla font-15">
                        {linkText}
                    </a>
                </Link>
            </div>
        </>
    )
}


const ContentInfo = ({ title = "", linkTitle = [] }) => {
    return (
        <>
            <div>
                <h3 className="color-gray font-w-700 font-karla font-15 m-b-56">
                    {title}
                </h3>
                <div>
                    {
                        linkTitle.map((e, i) => {
                            return (
                                <LinkTitle
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>

            </div>
        </>
    )
}

const LinkRs = ({ link = "", img = "" }) => {
    return (
        <>
            <div className="flex">
                <Link href={link}>
                    <a className="">
                        <img className="m-r-18 " src={`/image/${img}`} alt="" />
                    </a>
                </Link>
            </div>
        </>
    )
}

const Index = ({ img = "", text = "", linkRs = [], contentInfo = [] }) => {
    return (
        <>
            <footer className="p-v-20 bg-white-2">
                <div className="container p-h-15 flex m-v-140">
                    <div className="flex-12 flex-md-6 m-b-20">
                        <div>
                            <img className="flex  m-b-33" src={`/image/${img}`} alt="" />
                        </div>
                        <div>
                            <p className="color-gray font-karla font-16 m-b-38">
                                {text}
                            </p>
                            <div className="flex">
                                {
                                    linkRs.map((e, i) => {
                                        return (
                                            <LinkRs
                                                key={i}
                                                {...e}
                                            />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div className="flex-12 flex-md-6">
                        <div className="flex-justify-between flex">
                            {
                                contentInfo.map((e, i) => {
                                    return (
                                        <ContentInfo
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }
                        </div>
                    </div>
                </div>

            </footer>
        </>
    )
}
export default () => (<Index {...DATA} />)