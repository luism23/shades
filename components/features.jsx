import Link from "next/link"


const Index = ({ contentInfo = [], subTitle = "", title = "", text = "", link = "", linkText = "",img="" }) => {
    return (
        <>
            <div className="bg-blue-1 ">
                <div className="container p-h-15 flex flex-align-center p-v-130">
                    <div className="flex-6 d-none d-md-flex">
                        <img className="flex   " src={`/image/${img}`} alt="" />
                    </div>
                    <div className="flex-12 flex-md-6">
                        <div>
                            <h3 className="m-b-49 font-karla font-w-700 font-15 color-white">
                                {subTitle}
                            </h3>
                            <h1 className="m-b-16 font-karla font-w-700 font-48 color-white">
                                {title}
                            </h1>
                            <p className="m-b-51 font-karla font-18 color-white">
                                {text}
                            </p>
                            <div>
                                <Link href={link}>
                                    <a className="bg-white p-h-57 p-v-20 color-blue-1 border-style-solid border-0">
                                        {linkText}
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index