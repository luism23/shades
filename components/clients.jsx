
const ContentInfo = ({ img = "", text = "", names = "", namesCharge = "" }) => {
    return (
        <>
            <div className="flex-12 flex-sm-6 gap-30 bg-white m-b-30 p-v-30 p-h-38 ">
                <div className="m-b-18 flex">
                    <p className="font-karla font-18 color-black-1 ">
                        {text}
                    </p>
                </div>
                <div className="flex">
                    <div className="">
                        <img className="flex m-r-20" src={`/image/${img}`} alt="" />
                    </div>
                    <div className="">
                        <h3 className="font-karla font-17 font-w-700 color-black-1">
                            {names}
                        </h3>
                        <h4 className="font-karla font-15 color-black-1">
                            {namesCharge}
                        </h4>
                    </div>
                </div>
            </div>
        </>
    )
}


const Index = ({ title = "", contentInfo = [], subTitle = "", text = "" }) => {
    return (
        <>
            <div className="bg-white-2 p-b-122">
                <div className="container p-h-15 flex-gap-21  flex flex-justify-between">
                    <div className="flex-12 flex-md-4 flex-gap-21 flex flex-align-center">
                        <div className="">
                            <h3 className="m-b-49 font-karla font-w-700 font-15 color-blue-1">
                                {subTitle}
                            </h3>
                            <h1 className="m-b-16 font-karla font-w-700 font-48 color-black-1">
                                {title}
                            </h1>
                            <p className="m-b-51 font-karla font-18 color-gray">
                                {text}
                            </p>
                        </div>
                    </div>
                    <div className="flex-12 flex-sm-10 flex-md-8 flex-gap-21 ">
                        <div className="flex-justify-between flex flex-sm-gap-30" >
                                {
                                    contentInfo.map((e, i) => {
                                        return (
                                            <ContentInfo
                                                key={i}
                                                {...e}
                                            />
                                        )
                                    })
                                }
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index 