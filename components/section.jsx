

const ContentInfo = ({ title = "", text = "", img = "" }) => {
    return (
        <>
            <div className="flex  flex-12 flex-sm-7 flex-md-4 flex-md-gap-22 flex-nowrap m-b-20">
                <div className="width-60 ">
                    <img className="" src={`/image/${img}`} alt="" />
                </div>
                <div className="width-p-100">
                    <h3 className="font-21 fontw-700 font-karla m-b-7">
                        {title}
                    </h3>
                    <p className="font-17 color-gray font-karla">
                        {text}
                    </p>
                </div>

            </div>
        </>
    )
}


const Index = ({ contentInfo = [] }) => {
    return (
        <>
            <div className="bg-white-2 p-b-137 ">
                <div className="flex flex-md-gap-22 flex-justify-center flex-md-justify-between container p-h-15">
                    {
                        contentInfo.map((e, i) => {
                            return (
                                <ContentInfo
                                    key={i}
                                    {...e}
                                />
                            )
                        })
                    }
                </div>
            </div>
        </>
    )
}
export default Index