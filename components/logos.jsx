const Index = ({ img = "" }) => {
    return (
        <>
            <div className="bg-white-2 p-b-131">
                <div className="container p-h-15 ">
                    <img className="flex " src={`/image/${img}`} alt="" />
                </div>
            </div>
        </>
    )
}
export default Index