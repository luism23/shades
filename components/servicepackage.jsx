import Link from "next/link"

const Links = ({ link = "", linkText = "", color = "" }) => {
    return (
        <>
            <div className="">
                <Link href={link}>
                    <a className={`d-inline-block font-karla font-18 font-w-700  m-b-5 p-v-15 p-h-10 ${color}`}>{linkText}</a>
                </Link>
            </div>
        </>
    )
}

const Spamtext = ({ text = "" }) => {
    return (
        <>
            <li className="m-b-39">
                <span className="m-b-7 font-karla font-17 color-white " >
                    {text}
                </span>
            </li>
        </>
    )
}

const Plans = ({ subText = "", textNumber = "", text = "", links = [], spamtext = [], style = {} }) => {
    return (
        <>
            <div className="p-v-29 p-h-45 flex-md-gap-20 flex-12 flex-md-6 bg-blue-2 m-b-20" style={style}>
                <div>
                    <h3 className="font-karla font-13 font-w-700 color-white m-b-18">
                        {subText}
                    </h3>
                </div>
                <div className="">
                    <h2 className="font-karla font-50 font-w-700 color-white m-b-4">
                        {textNumber}
                    </h2>
                    <p className="font-karla font-14 color-white m-b-26">
                        {text}
                    </p>
                </div>
                <div>
                    <ul className="color-white" style={{ listStyle: "outside" }}>
                        {
                            spamtext.map((e, i) => {
                                return (
                                    <Spamtext
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </ul>
                </div>
                <div>
                    <div className="">
                        {
                            links.map((e, i) => {
                                return (
                                    <Links
                                        key={i}
                                        {...e}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

const Index = ({ plans = [], subTitle = "", title = "" }) => {
    return (
        <>
            <div className="bg-blue-3 p-v-158">
                <div className="container p-h-15">
                    <div className="text-center">
                        <h3 className="font-karla font-15 font-w-700 color-white m-b-49">
                            {subTitle}
                        </h3>
                        <h1 className="font-karla font-48 font-w-700 color-white m-b-93">
                            {title}
                        </h1>
                    </div>
                    <div className="flex">
                        <div className="flex flex-md-gap-20  flex-justify-between">

                            {

                                plans.map((e, i) => {
                                    return (
                                        <Plans
                                            key={i}
                                            {...e}
                                        />
                                    )
                                })
                            }

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index