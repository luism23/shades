import Link from "next/link"

const ContentInfo = ({ title = "", text = "", img = "" }) => {
    return (
        <>
            <div className="flex flex-gap-15 flex-12 flex-3 flex-gap-10 m-b-20 ">
                <div className="width-60 ">
                    <img className="" src={`/image/${img}`} alt="" />
                </div>
                <div className="width-p-100">
                    <h3 className="font-21 fontw-700 font-karla m-b-7">
                        {title}
                    </h3>
                    <p className="font-17 color-gray font-karla">
                        {text}
                    </p>
                </div>

            </div>
        </>
    )
}



const Index = ({ contentInfo = [], subTitle = "", title = "", text = "", img = "" }) => {
    return (
        <>
            <div className="bg-white-2 ">
                <div className="container p-h-15 flex  p-v-130">
                    <div className="flex-12 flex-md-6 flex">
                        <div>
                            <h3 className="m-b-49 font-karla font-w-700 font-15 color-blue-1">
                                {subTitle}
                            </h3>
                            <h1 className="m-b-16 font-karla font-w-700 font-48 color-black-1">
                                {title}
                            </h1>
                            <p className="m-b-51 font-karla font-18 color-gray">
                                {text}
                            </p>
                            <div className="">
                                <div className="flex flex-gap-22">
                                    {
                                        contentInfo.map((e, i) => {
                                            return (
                                                <ContentInfo
                                                    key={i}
                                                    {...e}
                                                />
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="flex-6 d-none d-md-flex">
                        <img className="flex" src={`/image/${img}`} alt="" />
                    </div>
                </div>
            </div>
        </>
    )
}
export default Index