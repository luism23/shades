const Index = ({ src = "", name = "", className = "" }) => {
    return (
        <>
            <img src={`/image/${src}`} alt={name} className={className} />
        </>
    );
};
export default Index;
