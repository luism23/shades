export default {
    img: "logo.png",
    linkTitle: [
        {
            link: "/",
            title: "Features",
        },
        {
            link: "/",
            title: "Services",
        },
        {
            link: "/",
            title: "Pricing",
        },
        {
            link: "/",
            title: "Contact",
        },
    ],
    link: "/",
    linkText: "Get Started",
}