export default {
    img: "logo.png",
    text: "Big, small, online, offline, local. Size doesn't matter. We work on diverse projects for top brands as well as for cool startups. ",
    linkRs: [
        {
            link: "/",
            img: "twi.png",
        },
        {
            link: "/",
            img: "face.png",
        },
        {
            link: "/",
            img: "google.png",
        },
    ],
    contentInfo: [
        {
            title: "Help menu",
            linkTitle: [
                {
                    link: "/",
                    linkText: "About",
                },
                {
                    link: "/",
                    linkText: "Features",
                },
                {
                    link: "/",
                    linkText: "Works",
                },
                {
                    link: "/",
                    linkText: "Career",
                },

            ]
        },
        {
            title: "Support",
            linkTitle: [
                {
                    link: "/",
                    linkText: "Contact",
                },
                {
                    link: "/",
                    linkText: "Help & Support",
                },
                {
                    link: "/",
                    linkText: "Privacy Policy",
                },
                {
                    link: "/",
                    linkText: "Terms & Conditions",
                },

            ]
        },
        {
            title: "Products",
            linkTitle: [
                {
                    link: "/",
                    linkText: "Shades Pro",
                },
                {
                    link: "/",
                    linkText: "Essential Blocks",
                },
                {
                    link: "/",
                    linkText: "Avasta Dash",
                },
                {
                    link: "/",
                    linkText: "vApp Landing Page",
                },

            ]
        },

    ],
}