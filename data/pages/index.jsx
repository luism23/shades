export default {
    banner: {
        subTitle: "Present your service",
        title: "Make landing page fast and easily.",
        text: "Create custom landing pages with Shades that convert more visitors than any website—no coding required.",
        link: "/",
        linkText: "Get Started",
        text1: "Trused by over 50,000+ customers",
    },
    section: {
        contentInfo: [
            {
                img: "lapiz.png",
                title: "Easy to use",
                text: "Editing and customizing Essential Landing is easy and fast.",
            },
            {
                img: "libro.png",
                title: "300+ Blocks",
                text: "Editing and customizing Essential Landing is easy and fast.",
            },
            {
                img: "monitor.png",
                title: "100% Responsive",
                text: "Editing and customizing Essential Landing is easy and fast.",
            },
        ]
    },
    features: {
        img: "contentInfo.png",
        subTitle: "Design is everything",
        title: "Present your app features like this way.",
        text: "We designed and tested prototypes that helped identify pain points in the account creation process. Together, we shaped the new standard.",
        link: "/",
        linkText: "Get Started",
    },
    features2: {
        subTitle: "Design is everything",
        title: "Present your app features like this way.",
        text: "We designed and tested prototypes that helped identify pain points in the account creation process. Together, we shaped the new standard.",
        contentInfo: [
            {
                img: "lapiz1.png",
                title: "Easy to use",
                text: "Event is not like most tech conferences. We want our design to engage.",
            },
            {
                img: "seccion.png",
                title: "Beautiful Sections",
                text: "Event is not like most tech conferences. We want our design to engage.",
            },
        ],
        img: "features2.png"
    },
    clients: {
        subTitle: "Customers’ story",
        title: "50k+ users are showing love.",
        text: "We designed and tested prototypes that helped identify pain points in the account creation process. Together, we shaped the new standard.",
        contentInfo: [
            {
                text: "“You made it so simple. My new site is so much faster and easier to work with than my old site. I just choose the page, make the change and click save.",
                img: "name1.png",
                names: "Cameron Anderson",
                namesCharge: "Designer",
            },
            {
                text: "“Simply the best. Better than all the rest. I’d recommend this product to beginners and advanced users.”",
                img: "name2.png",
                names: "Leo Morton",
                namesCharge: "Marketer",
            },
            {
                text: "“Finally, I’ve found a template that covers all bases for a bootstrapped startup. We were able to launch in days, not months.”",
                img: "name3.png",
                names: "Catherine Simmons",
                namesCharge: "Marketer",
            },
            {
                text: "With Shades, it’s quicker with the customer, the customer is more ensured of getting exactly what they ordered, and I’m all for the efficiency.",
                img: "name4.png",
                names: "Harvey Ingram",
                namesCharge: "UX Designer",
            },
        ]
    },
    logos: {
        img: "logos.png",
    },
    servicepackage:{
        subTitle:"It’s time to taste it",
        title:"Ready to use our service? Choose a package.",
        plans:[
            {
                
                subtext:"Basic",
                textNumber:"$ 19 / month",
                text:"Good for small business launching their products less then once a year",
                spamtext:[
                    {
                        text:"Unlimited Blocks"
                    },
                    {
                        text:"5GB Clould Storages"
                    },
                    {
                        text:"Custom Domain Names"
                    },
                    {
                        text:"Unlimited Emails"
                    },
                ],
                links:[
                    {
                        link:"/",
                        linkText:"Start Free Trial",
                        color:"bg-blue-1 color-white",
                    },
                    {
                        link:"/",
                        linkText:"No credit card required",
                        color:"bg-transparent color-gray",
                    },
                ]
            },
            {
                style:{
                    "--blue-2":"white",
                    "--white":"black-1",
                    "--blue-1":"orangle",
                },
                subText:"Pro",
                textNumber:"$ 29 / month",
                text:"Good for small business launching their products less then once a year",
                spamtext:[
                    {
                        text:"Unlimited Blocks"
                    },
                    {
                        text:"5GB Clould Storages"
                    },
                    {
                        text:"Custom Domain Names"
                    },
                    {
                        text:"Unlimited Emails"
                    },
                ],
                links:[
                    {
                        link:"/",
                        linkText:"Start Free Trial",
                        color:"bg-orangle color-white-3",
                    },
                    {
                        link:"/",
                        linkText:"No credit card required",
                        color:"bg-transparent color-gray",
                    },
                ]
            }
        ],
    }
}